create table Users(
	Users_ID integer,
	last_name varchar2(20),
	gender varchar2(10),
	first_name varchar2(40),
	yob integer,
	mob integer,
	dob integer,
	primary key(Users_ID)
);

create table School(
	School_ID integer,
	name varchar2(100),
	year integer,
	degree varchar2(20),
	major varchar2(100),
	primary key(School_ID)
);

create table Event(
	type varchar2(20),
	subtype varchar2(20),
	description varchar2(100),
	Etag varchar2(20),
	Ename varchar2(20),
	Eid integer,
	Stime date,
	Etime date,
	host varchar2(40),
	location varchar2(40),
	primary key(Eid)
);

create table Location(
	Lid integer,
	city varchar2(20),
	country varchar2(20),
	state varchar2(20),
	primary key(Lid)
);

create table Albums(
	name varchar2(20),
	Ctime date,
	Aid integer,
	Mtime date,
	visibility varchar2(20),
	link varchar2(100),
	primary key(Aid)
);

create table Photo(
	Mtime date,
	Ctime date,
	caption varchar2(20),
	link varchar2(100),
	Pid integer,
	primary key(Pid)
);

create table Tag(
	x integer,
	y integer,
	time date,
	Sid integer not null,
	Pid integer not null,
	primary key(Sid, Pid),
	foreign key(Pid)
		references Photo
		on delete cascade
);

create table Comments(
	time date,
	content varchar2(100),
	Pid integer,
	Users_ID integer,
	primary key(Pid, Users_ID, time),
	foreign key(Pid)
		references Photo
		on delete cascade,
	foreign key(Users_ID)
		references Users
);

create table study(
	School_ID integer,
	Users_ID integer,
	primary key(School_ID, Users_ID),
	foreign key(School_ID)
		references School,
	foreign key(Users_ID)
		references Users
);

create table friend(
	Users1_ID integer,
	Users2_ID integer,
	primary key(Users1_ID, Users2_ID),
	foreign key(Users1_ID)
		references Users,
	foreign key(Users2_ID)
		references Users
);

create table participate(
	status varchar2(15),
	Eid integer,
	Users_ID integer,
	primary key(Eid, Users_ID),
	foreign key(Users_ID)
		references Users,
	foreign key(Eid)
		references Event
);

create table creates(
	Eid integer,
	Users_ID integer,
	primary key(Eid),
	foreign key(Eid)
		references Event,
	foreign key(Users_ID)
		references Users
);

create table happens(
	Eid integer,
	Lid integer,
	primary key(Eid, Lid),
	foreign key(Eid)
		references Event,
	foreign key(Lid)
		references Location
);

create table hometown(
	Users_ID integer,
	Lid integer,
	primary key(Lid, Users_ID),
	foreign key(Lid)
		references Location,
	foreign key(Users_ID)
		references Users
);

create table livein(
	Users_ID integer,
	Lid integer,
	primary key(Lid, Users_ID),
	foreign key(Lid)
		references Location,
	foreign key(Users_ID)
		references Users
);

create table have(
	Aid integer,
	Users_ID integer,
	primary key(Aid),
	foreign key(Aid)
		references Albums,
	foreign key(Users_ID)
		references Users
);

create table cover(
	Aid integer,
	Pid integer,
	primary key(Aid),
	foreign key(Aid)
		references Albums,
	foreign key(Pid)
		references Photo
);

create table belongs(
	Aid integer,
	Pid integer,
	primary key(Pid),
	foreign key(Aid)
		references Albums,
	foreign key(Pid)
		references Photo
);