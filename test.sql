select P.photo_id, U1.user_id, U1.first_name, U1.last_name, U1.year_of_birth, U2.user_id, U2.first_name, U2.last_name, U2.year_of_birth, P.album_id, P.photo_caption, P.photo_link, A.album_name from yjtang.PUBLIC_USERS U1, yjtang.PUBLIC_USERS U2, yjtang.PUBLIC_PHOTOS P, yjtang.PUBLIC_TAGS T1, yjtang.PUBLIC_TAGS T2, yjtang.PUBLIC_ALBUMS A  where U1.gender = 'female' and U2.gender = 'male' and P.album_id = A.album_id and abs(U1.year_of_birth - U2.year_of_birth) <= 2 and (U1.user_id in (select user1_id from yjtang.PUBLIC_FRIENDS ) and U2.user_id not in (select user2_id from yjtang.PUBLIC_FRIENDS ) and U2.user_id not in (select user2_id from yjtang.PUBLIC_FRIENDS F where F.user1_id = U1.user_id))and(U2.user_id in (select user1_id from yjtang.PUBLIC_FRIENDS )and U1.user_id not in(select user2_id from yjtang.PUBLIC_FRIENDS F  where F.user1_id = U2.user_id))  and U1.user_id = T1.tag_subject_id and U2.user_id = T2.tag_subject_id and T1.tag_photo_id = T2.tag_photo_id and P.photo_id = T1.tag_photo_id  group by P.photo_id, U1.user_id, U1.first_name, U1.last_name, U1.year_of_birth, U2.user_id, U2.first_name, U2.last_name, U2.year_of_birth, P.album_id, P.photo_caption, P.photo_link, A.album_name  order by count(P.photo_id) desc, U1.user_id asc, U2.user_id asc 

select P.photo_id, U1.user_id, U1.first_name, U1.last_name, U1.year_of_birth, U2.user_id, U2.first_name, U2.last_name, U2.year_of_birth, P.album_id, P.photo_caption, P.photo_link, A.album_name from
yjtang.PUBLIC_USERS U1, yjtang.PUBLIC_USERS U2, yjtang.PUBLIC_PHOTOS P, yjtang.PUBLIC_TAGS T1, yjtang.PUBLIC_TAGS T2, yjtang.PUBLIC_ALBUMS A
where U1.gender = 'female' and U2.gender = 'male' and A.album_id = P.album_id and abs(U1.year_of_birth - U2.year_of_birth) <= 2 and
(
	U1.user_id in 
	(
		select user1_id from
		yjtang.PUBLIC_FRIENDS
	)
	and
	U2.user_id not in
	(
		select user2_id from
		yjtang.PUBLIC_FRIENDS F
		where F.user1_id = U1.user_id
	)	
)
and
(
	U2.user_id in 
	(
		select user1_id from
		yjtang.PUBLIC_FRIENDS
	)
	and
	U1.user_id not in
	(
		select user2_id from
		yjtang.PUBLIC_FRIENDS F
		where F.user1_id = U2.user_id
	)	
)
and U1.user_id = T1.tag_subject_id and U2.user_id = T2.tag_subject_id and T1.tag_photo_id = T2.tag_photo_id and P.photo_id = T1.tag_photo_id
group by P.photo_id, U1.user_id, U1.first_name, U1.last_name, U1.year_of_birth, U2.user_id, U2.first_name, U2.last_name, U2.year_of_birth, P.album_id, P.photo_caption, P.photo_link, A.album_name
order by count(P.photo_id) desc, U1.user_id asc, U2.user_id asc;